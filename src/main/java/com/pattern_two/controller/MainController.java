package com.pattern_two.controller;

import com.pattern_two.model.Admin;
import com.pattern_two.model.Developer;
import com.pattern_two.model.Task;
import com.pattern_two.model.TasksHolder;
import com.pattern_two.model.Team;

import com.pattern_two.view.View;
import java.util.List;
import java.util.Scanner;

public class MainController {

    private TasksHolder tasksHolder;
    private Team team;
    List<Task> tasks;
    private Scanner sc = new Scanner(System.in);
    private View view = new View();


    public MainController() {
        tasksHolder = new TasksHolder();
        team = new Team(tasksHolder);
        tasks = tasksHolder.getTasks();
    }

    public void run() {
        while (true) {
            view.printUsers(team);
            int userChoice = sc.nextInt() - 1;
            if (userChoice == 0) {
                adminMenu();
            } else if (userChoice > 0 && userChoice <= team.getDevelopers().size()) {
                Developer currentDeveloper = team.getDevelopers().get(userChoice - 1);
                developerMenu(currentDeveloper);
            }
        }
    }

    private void adminMenu() {
        boolean continueWorking = true;
        while (continueWorking){
        Admin admin = team.getAdmin();
        view.printAdminMethods();
        int menuChoice = sc.nextInt();
        switch (menuChoice) {
            case 1: {
                view.printNewTask();
                sc.nextLine();//for not getting empty line
                String taskName = sc.nextLine();
                Task task = new Task(taskName);
                admin.addInToDo(task);
                break;
            }
            case 2: {
                view.printTasks(tasks);
                int taskIndex = sc.nextInt() - 1;
                admin.returnInToDo(tasks.get(taskIndex));
                break;
            }
            case 3: {
                view.printTasks(tasks);
                int taskIndex = sc.nextInt() - 1;
                admin.addInDone(tasks.get(taskIndex));
                break;
            }
            case 4: {
                view.printTasks(tasks);
                int taskIndex = sc.nextInt() - 1;
                admin.addInBlocked(tasks.get(taskIndex));
                break;
            }
            default: { return;}
        }
        view.doContinue();
        if (sc.nextInt() == 2) {
            continueWorking=false;
        }
        }
    }

    private void developerMenu(Developer currentDeveloper) {
        boolean continueWorking = true;
        while (continueWorking){
        view.printDeveloperMethods();
        int methodIndex = sc.nextInt();
        switch (methodIndex) {
            case 1: {
                view.chooseTaskForAddingInProgress();
                view.printTasks(tasks);
                int taskIndex = sc.nextInt() - 1;
                currentDeveloper.addInProgress(tasks.get(taskIndex));
                break;
            }
            case 2: {
                currentDeveloper.addInCodeReview();
                break;
            }
            case 3: {
                currentDeveloper.addInTest();
                break;
            }
            case 4: {
                currentDeveloper.addInVerification();
                break;
            }
        }
            view.doContinue();
            if (sc.nextInt() == 2) {
                continueWorking=false;
            }
        }
    }
}
