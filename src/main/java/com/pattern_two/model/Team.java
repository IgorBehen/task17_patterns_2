package com.pattern_two.model;

import java.util.ArrayList;
import java.util.List;

public class Team {
    private List<Developer> developers;
    private Admin admin;

    public Team(TasksHolder tasksHolder) {
        developers = new ArrayList<>();
        admin = new Admin("Taras", tasksHolder);
        initDevelopers();
    }

    private void initDevelopers(){
        DevelopersEnum[] values = DevelopersEnum.values();
        for (DevelopersEnum value : values) {
            developers.add(new Developer(value.name()));
        }
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public Admin getAdmin() {
        return admin;
    }
}
