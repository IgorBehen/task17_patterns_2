package com.pattern_two.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TasksHolder {

    private List<Task> tasks;

    {
        tasks = new ArrayList<>();
        tasks.add(new Task("task1"));
        tasks.add(new Task("task2"));
        tasks.add(new Task("task3"));
        tasks.add(new Task("task4"));
        tasks.add(new Task("task5"));
    }

    public List<Task> getTasksByStates(State... states) {
        return tasks.stream().filter(task -> instancesOfStates(task, states))
            .collect(Collectors.toList());
    }

    private boolean instancesOfStates(Task task, State... states) {
        for (State state : states) {
            if (state.getStateName().equals(task.getTaskState().getStateName())) {
                return true;
            }
        }
        return false;
    }

    public List<Task> getTasksBy(State state){
        return tasks.stream()
                .filter(task -> task.getTaskState().getStateName().equals(state.getStateName()))
                .collect(Collectors.toList());
    }
//    public List<Task> getTasksForReturnToToDo(){
//        return tasks.stream()
//                .filter(this::isAvailableForReturnToToDo)
//                .collect(Collectors.toList());
//    }
//
//    private boolean isAvailableForReturnToToDo(Task task){
//        return task.getTaskState().getStateName().equals("ProgressState") ||
//                task.getTaskState().getStateName().equals("CodeReviewState") ||
//                task.getTaskState().getStateName().equals("TestState") ||
//                task.getTaskState().getStateName().equals("VerificationState");
//    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
