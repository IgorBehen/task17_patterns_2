package com.pattern_two.model.state_impl;

import com.pattern_two.model.State;
import com.pattern_two.model.Task;
import com.pattern_two.model.state_impl.BlockedState;
import com.pattern_two.model.state_impl.ProgressState;
import com.pattern_two.model.state_impl.TestState;

public class CodeReviewState implements State {

    @Override
    public void addInTest(Task task) {
        task.setTaskState(new TestState());
        System.out.println("Added In Test!");
    }

    @Override
    public void addInProgress(Task task) {
        task.setTaskState(new ProgressState());
        System.out.println("Added In Progress!");
    }

    @Override
    public void addInBlocked(Task task) {
        task.setTaskState(new BlockedState());
        System.out.println("Added In Blocked!");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String getStateName() {
        return "CodeReviewState";
    }
}


