package com.pattern_two.model;

public interface State {

    default String getStateName(){
        return "State";
    }

    default void addInToDo(Task task) {
        System.out.println("addInToDo- is not allowed");
    }
    default void addInProgress(Task task) {
        System.out.println("addInProgress - is not allowed");
    }
    default void addInCodeReview(Task task) {
        System.out.println("addInCodeReview - is not allowed");
    }
    default void addInTest(Task task) {
        System.out.println("addInTest - is not allowed");
    }
    default void addInDone(Task task) {
        System.out.println("addInDone - is not allowed");
    }
    default void addInVerification(Task task){ System.out.println("addInVerification - is not allowed"); }
    default void addInBlocked(Task task) { System.out.println("addInBlocked - is not allowed"); }
}
