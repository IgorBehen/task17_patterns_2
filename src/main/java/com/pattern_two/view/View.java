package com.pattern_two.view;

import com.pattern_two.model.Developer;
import com.pattern_two.model.Task;
import com.pattern_two.model.Team;

import java.util.List;

public class View {
    
    public void printUsers(Team team){
        List<Developer> developers = team.getDevelopers();
        System.out.println("Choose user");
        System.out.println("1 " + team.getAdmin().getName() + "(Administrator)");
        for (int i = 0; i < developers.size(); i++) {
            System.out.println((i + 2) + " " + developers.get(i).getName());
        }
    }

    public void doContinue(){
        System.out.println("If you want keep working enter 1, if you want to stop enter 2");
    }

    public void printAdminMethods(){
        System.out.println("1) Add new task in ToDo\n" +
                "2) Return task in ToDo\n" +
                "3) Add task in Done\n" +
                "4) Add task in Blocked\n" +
                "0) Quit");
    }
    public void printNewTask(){
        System.out.println("Please, tap new task");
    }

    public void printTasks(List<Task> tasks){
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i+1) + " " + tasks.get(i));

        }
    }
    public void printDeveloperMethods(){
        System.out.println("1) Add task in Progress\n"
            + "2) Add task in Code Review\n"
            + "3) Add task in Test\n"
            + "4) Add task in Verification");
    }

    public void chooseTaskForAddingInProgress(){
        System.out.println("Choose task to add in progress");
    }

}
